# dns-over-https

High performance DNS over HTTPS client & server package built with fpm and GitLab CI.

[Source code](https://github.com/m13253/dns-over-https)

## Download

Download dns-over-https server package for:

- [Ubuntu/Debian package amd64](https://gitlab.com/libreops/packages/dns-over-https/-/jobs/artifacts/master/browse?job=run-build)

## Install

```bash
sudo apt-get -y install doh-server*amd64.deb

```
